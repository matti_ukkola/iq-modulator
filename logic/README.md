# IQ Modulator Logic

## Setup

1. `rm -r IQModulator/`
2. Open libero, do not open any project
3. Run script `setup.tcl` from Project->Execute Script


## Update

1. Get new steps from libero script export tool
2. Update `setup.tcl` accordingly
3. Update `layout.pdf` in libero and compress the file


## Simulation with GHDL

Build
```sh
mkdir build
cd build
ghdl -a ../hdl/heartbeat.vhd
ghdl -e heartbeat
```

Run
```sh
ghdl -r heartbeat --wave=wave_heartbeat.ghw
<Ctrl-c> to stop
```

Allow `X11` connection from `podman` to `host`:
```sh
xhost +"local:podman@" #
```

Display waveform:
```sh
gtkwave wave_heartbeat.ghw
```
