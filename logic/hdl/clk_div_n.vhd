-- Divides clock by an even number N
--
-- Targeted device: <Family::SmartFusion2> <Die::M2S010> <Package::144 TQ>
-- Author: Matti Ukkola


library IEEE;

use IEEE.std_logic_1164.all;

entity clk_div_n is
  generic(
    -- note: N must be divisible by 2
    constant N : integer := 10 -- 1 Hz ,50_000_000, for oszi: 100/500khz-50/1Mhz-25/2Mhz-10/10Mhz-5/20Mhz-2/50Mhz
  );
  port(
    clk     : in  std_logic;
    reset_n : in  std_logic;
    clk_out : out std_logic);
end clk_div_n;

architecture architecture_clk_div_n of clk_div_n is
  signal clk_out_sig : std_logic;
  constant max_count : integer := (N/2 - 1);
begin
  process(clk, reset_n)
    variable counter : integer range 0 to max_count := 0;
  begin
    if(reset_n = '0') then
      counter := 0;
      clk_out_sig <= '0';
    elsif(clk'EVENT and clk = '1') then
      if(counter = 0) then
        clk_out_sig <= not clk_out_sig;
      end if;
      if (counter = max_count) then
        counter := 0;
      else
        counter := counter + 1;
      end if;
    end if;
  end process;
  clk_out <= clk_out_sig;
end architecture_clk_div_n;