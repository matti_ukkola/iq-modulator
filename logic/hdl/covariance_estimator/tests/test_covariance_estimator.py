import math
import cocotb
from cocotb.triggers import RisingEdge
from cocotb.clock import Clock

from helpers import to_fixed, to_float

INITIAL_VARIANCE_PHASE = 1 / 12
INITIAL_VARIANCE_FREQUENCY = 1 / 2**10

VARIANCE_PHASE = 1 / 2**10
VARIANCE_FREQUENCY = 1 / 2**10


async def init(dut):
    c = Clock(dut.clk, 10, "ns")
    await cocotb.start(c.start())
    print(f"Running {c}")
    dut.rst.value = 1
    await RisingEdge(dut.clk)
    await RisingEdge(dut.clk)
    dut.rst.value = 0


@cocotb.test()
async def test_init(dut):
    await init(dut)
    assert dut.apriori_00.value == 0
    assert dut.apriori_01.value == 0
    assert dut.apriori_10.value == 0
    assert dut.apriori_11.value == 0
    assert dut.apriori_valid.value == 0

    exp_initial_variance_phase = to_float(
        to_fixed(INITIAL_VARIANCE_PHASE, dut.n_fractional_bits_00.value),
        dut.n_fractional_bits_00.value,
    )
    assert (
        to_float(dut.aposteriori_00.value, dut.n_fractional_bits_00.value)
        == exp_initial_variance_phase
    )
    assert dut.aposteriori_01.value == 0
    assert dut.aposteriori_10.value == 0
    assert (
        to_float(dut.aposteriori_11.value, dut.n_fractional_bits_11.value)
        == VARIANCE_FREQUENCY
    )
    assert dut.aposteriori_valid.value == 0


@cocotb.test()
async def test_state_machine(dut):
    await init(dut)
    await RisingEdge(dut.clk)  # calculate a priori
    dut.input_valid.value = 1
    await RisingEdge(dut.clk)  # a priori ready, calculate a posteriori
    dut.input_valid.value = 0
    assert dut.apriori_valid.value == 1
    assert dut.aposteriori_valid.value == 0
    await RisingEdge(dut.clk)  # a posteriori ready, calculate a priori
    assert dut.apriori_valid.value == 0
    assert dut.aposteriori_valid.value == 1
    await RisingEdge(dut.clk)  # a priori ready, wait for input!
    assert dut.apriori_valid.value == 1
    assert dut.aposteriori_valid.value == 0
    await RisingEdge(dut.clk)  # still wait for input!
    assert dut.apriori_valid.value == 1
    assert dut.aposteriori_valid.value == 0


@cocotb.test()
async def test_first_apriori_estimation(dut):
    await init(dut)
    await RisingEdge(dut.clk)
    assert dut.apriori_valid.value == 0
    await RisingEdge(dut.clk)
    assert dut.apriori_valid.value == 1

    res_apriori_00 = to_float(dut.apriori_00.value, dut.n_fractional_bits_00.value)

    assert math.isclose(
        res_apriori_00,
        INITIAL_VARIANCE_PHASE + INITIAL_VARIANCE_FREQUENCY + VARIANCE_PHASE,
        abs_tol=1e-3,
    )
    assert math.isclose(
        res_apriori_00,
        INITIAL_VARIANCE_PHASE + INITIAL_VARIANCE_FREQUENCY + VARIANCE_PHASE,
        rel_tol=1e-2,
    )
    exp_aprior_00 = to_float(
        to_fixed(
            INITIAL_VARIANCE_PHASE + VARIANCE_PHASE + INITIAL_VARIANCE_FREQUENCY,
            dut.n_fractional_bits_00.value,
        ),
        dut.n_fractional_bits_00.value,
    )
    assert res_apriori_00 == exp_aprior_00

    assert (
        to_float(dut.apriori_01.value, dut.n_fractional_bits_01.value)
        == INITIAL_VARIANCE_FREQUENCY
    )
    assert (
        to_float(dut.apriori_10.value, dut.n_fractional_bits_01.value)
        == INITIAL_VARIANCE_FREQUENCY
    )
    assert (
        to_float(dut.apriori_11.value, dut.n_fractional_bits_11.value)
        == INITIAL_VARIANCE_FREQUENCY + VARIANCE_FREQUENCY
    )

    await RisingEdge(dut.clk)
    # keep value until kalman gain is ready
    assert dut.apriori_valid.value == 1


@cocotb.test()
async def test_first_full_estimation(dut):
    await init(dut)
    await RisingEdge(dut.clk)
    assert dut.apriori_valid.value == 0
    dut.kalman_gain_phase.value = 0
    dut.kalman_gain_frequency.value = 0
    dut.input_valid.value = 1

    await RisingEdge(dut.clk)
    assert dut.apriori_valid.value == 1
    dut.input_valid.value = 0
    res_apriori_00 = to_float(dut.apriori_00.value, dut.n_fractional_bits_00.value)
    res_apriori_11 = to_float(dut.apriori_11.value, dut.n_fractional_bits_11.value)

    await RisingEdge(dut.clk)
    assert dut.aposteriori_valid.value == 1
    res_aposteriori_00 = to_float(
        dut.aposteriori_00.value, dut.n_fractional_bits_00.value
    )
    assert (
        res_aposteriori_00 == res_apriori_00
    ), "Kalman gain for phase is 0, no change expected"
    assert (
        to_float(dut.aposteriori_01.value, dut.n_fractional_bits_01.value)
        == INITIAL_VARIANCE_FREQUENCY
    )
    assert (
        to_float(dut.aposteriori_10.value, dut.n_fractional_bits_10.value)
        == INITIAL_VARIANCE_FREQUENCY
    )
    res_aposteriori_11 = to_float(
        dut.aposteriori_11.value, dut.n_fractional_bits_11.value
    )
    assert (
        res_aposteriori_11 == res_apriori_11
    ), "Kalman gain for frequency is 0, no change expected"


@cocotb.test()
async def test_convergence_11_no_gain(dut):
    await init(dut)
    await RisingEdge(dut.clk)
    dut.kalman_gain_phase.value = 0
    dut.kalman_gain_frequency.value = 0
    dut.input_valid.value = 1

    for i in range(1000):
        await RisingEdge(dut.clk)
        assert dut.apriori_valid.value == 1
        apriori_11_i = to_float(dut.apriori_11.value, dut.n_fractional_bits_11.value)
        assert apriori_11_i == (i + 1) * VARIANCE_FREQUENCY + INITIAL_VARIANCE_FREQUENCY
        await RisingEdge(dut.clk)
    assert apriori_11_i < 2**dut.n_integer_bits_11.value
    exp_final_val = 1000 * INITIAL_VARIANCE_FREQUENCY + INITIAL_VARIANCE_FREQUENCY
    assert exp_final_val == 0.9775390625
    assert apriori_11_i == exp_final_val


@cocotb.test()
async def test_convergence_01_and_10_no_gain(dut):
    await init(dut)
    await RisingEdge(dut.clk)
    dut.kalman_gain_phase.value = 0
    dut.kalman_gain_frequency.value = 0
    dut.input_valid.value = 1

    for i in range(1000):
        await RisingEdge(dut.clk)
        assert dut.apriori_valid.value == 1
        apriori_01_i = to_float(dut.apriori_01.value, dut.n_fractional_bits_01.value)
        exp_apriori_01_i = (
            i * (i + 1) // 2 * VARIANCE_FREQUENCY + (i + 1) * INITIAL_VARIANCE_FREQUENCY
        )  # triangular numbers
        assert apriori_01_i == exp_apriori_01_i, f"i={i}"
        assert dut.apriori_01.value == dut.apriori_10.value
        await RisingEdge(dut.clk)
    print(
        f"gain=0, i={i}, apriori_01_i: {apriori_01_i}, exp_apriori_01_i: {exp_apriori_01_i}"
    )
    assert apriori_01_i < 2**dut.n_integer_bits_01.value
    assert apriori_01_i == 488.76953125


@cocotb.test()
async def test_convergence_00_no_gain(dut):
    await init(dut)
    await RisingEdge(dut.clk)
    dut.kalman_gain_phase.value = 0
    dut.kalman_gain_frequency.value = 0
    dut.input_valid.value = 1

    constant_phase = to_float(
        to_fixed(
            INITIAL_VARIANCE_PHASE,
            dut.n_fractional_bits_00.value,
        ),
        dut.n_fractional_bits_00.value,
    )

    for i in range(1000):
        await RisingEdge(dut.clk)
        assert dut.apriori_valid.value == 1
        apriori_00_i = to_float(dut.apriori_00.value, dut.n_fractional_bits_00.value)

        linear = (i + 1) * (VARIANCE_PHASE + INITIAL_VARIANCE_FREQUENCY)
        triangular_frequency = (
            i * (i + 1) // 2 * VARIANCE_FREQUENCY
            + 2 * i * (i + 1) // 2 * INITIAL_VARIANCE_FREQUENCY
        )
        double_tetrahedral_frequency = (
            2 * i * (i - 1) * (i + 1) // 6
        ) * VARIANCE_FREQUENCY

        exp_apriori_00_i = (
            double_tetrahedral_frequency
            + triangular_frequency
            + linear
            + constant_phase
        )
        assert apriori_00_i == exp_apriori_00_i, (
            f"i={i}, constant_phase: {constant_phase}, linear: {linear}, "
            f"triangular_frequency: {triangular_frequency}, "
            f"tetrahedral_frequency: {double_tetrahedral_frequency}"
        )
        await RisingEdge(dut.clk)

    print(
        f"gain=0, i={i}, apriori_00_i: {apriori_00_i}, exp_apriori_00_i: {exp_apriori_00_i}"
    )
    assert apriori_00_i < 2**dut.n_integer_bits_00.value
    assert apriori_00_i == 326010.3369140625


@cocotb.test()
async def test_convergence_max_gain(dut):
    await init(dut)
    await RisingEdge(dut.clk)
    dut.kalman_gain_phase.value = to_fixed(
        1, dut.n_fractional_bits_kalman_gain_phase.value
    )
    dut.kalman_gain_frequency.value = to_fixed(
        1, dut.n_fractional_bits_kalman_gain_frequency.value
    )
    dut.input_valid.value = 1

    for i in range(1000):
        await RisingEdge(dut.clk)
        assert dut.apriori_valid.value == 1
        assert to_float(dut.apriori_11.value, dut.n_fractional_bits_11.value) == (
            INITIAL_VARIANCE_FREQUENCY + VARIANCE_FREQUENCY
            if i == 0
            else VARIANCE_FREQUENCY
        ), f"i={i}, gain is 1, expect to start at initial value and then to var_f"

        assert to_float(dut.apriori_01.value, dut.n_fractional_bits_01.value) == (
            INITIAL_VARIANCE_FREQUENCY if i == 0 else 0
        ), f"i={i}, gain is 1, expect to start at initial value and then go to 0"
        assert dut.apriori_10.value == dut.apriori_01.value

        apriori_00_i = to_float(dut.apriori_00.value, dut.n_fractional_bits_00.value)
        exp_apriori_00_i = to_float(
            to_fixed(
                INITIAL_VARIANCE_PHASE
                + INITIAL_VARIANCE_FREQUENCY
                + VARIANCE_FREQUENCY,
                dut.n_fractional_bits_00.value,
            ),
            dut.n_fractional_bits_00.value,
        )
        assert apriori_00_i == (
            exp_apriori_00_i
            if i == 0
            else to_float(
                to_fixed(VARIANCE_PHASE, dut.n_fractional_bits_00.value),
                dut.n_fractional_bits_00.value,
            )
        ), f"i={i}, gain is 1, expect to start at initial value and then go to var_phase"

        await RisingEdge(dut.clk)
