library ieee;
use ieee.std_logic_1164.all;
use ieee.fixed_pkg.all;
use ieee.math_real.all;

entity covariance_estimator is
  generic(
    initial_variance_phase                  : real    := 1.0/12.0;
    initial_variance_frequency              : real    := 1.0/2.0**10;
    -- assume uniform distribution for phase with T = 1
    variance_phase                          : real    := 1.0/2.0**10;
    -- assume small variation in frequency
    variance_frequency                      : real    := 1.0/2.0**10;
    n_integer_bits_kalman_gain_phase        : natural := 1;
    n_fractional_bits_kalman_gain_phase     : natural := 10;
    n_integer_bits_kalman_gain_frequency    : natural := 1;
    n_fractional_bits_kalman_gain_frequency : natural := 10;

    -- state precision
    n_integer_bits_00                       : natural := 20;
    n_integer_bits_01                       : natural := 10;
    n_integer_bits_10                       : natural := 10;
    n_integer_bits_11                       : natural := 0;
    n_fractional_bits_00                    : natural := 10;
    n_fractional_bits_01                    : natural := 10;
    n_fractional_bits_10                    : natural := 10;
    n_fractional_bits_11                    : natural := 10
  );
  port(
    rst                                 : in          std_logic;
    clk                                 : in          std_logic;
    kalman_gain_phase                   : in          ufixed(n_integer_bits_kalman_gain_phase - 1 downto -n_fractional_bits_kalman_gain_phase);
    kalman_gain_frequency               : in          ufixed(n_integer_bits_kalman_gain_frequency - 1 downto -n_fractional_bits_kalman_gain_frequency);
    input_valid                         : in          std_logic;
    -- a priori covariance estimation
    apriori_00                          : out         ufixed(n_integer_bits_00 - 1 downto -n_fractional_bits_00);
    apriori_01                          : out         ufixed(n_integer_bits_01 - 1 downto -n_fractional_bits_01);
    apriori_10                          : out         ufixed(n_integer_bits_10 - 1 downto -n_fractional_bits_10);
    apriori_11                          : out         ufixed(n_integer_bits_11 - 1 downto -n_fractional_bits_11);
    apriori_valid                       : out         std_logic;
    -- a posteriori covariance estimation
    aposteriori_00                      :         out ufixed(n_integer_bits_00 - 1 downto -n_fractional_bits_00);
    aposteriori_01                      :         out ufixed(n_integer_bits_01 - 1 downto -n_fractional_bits_01);
    aposteriori_10                      :         out ufixed(n_integer_bits_10 - 1 downto -n_fractional_bits_10);
    aposteriori_11                      :         out ufixed(n_integer_bits_11 - 1 downto -n_fractional_bits_11);
    aposteriori_valid                   : out         std_logic
  );
end entity;

architecture covariance_estimator_arch of covariance_estimator is
  constant initial_variance_phase_ufixed     : ufixed := to_ufixed(initial_variance_phase, -1, -10);
  constant rel_conv_error                    : real   := abs(to_real(initial_variance_phase_ufixed) - initial_variance_phase)/abs(initial_variance_phase);
  constant initial_variance_frequency_ufixed : ufixed := to_ufixed(initial_variance_frequency, integer(log2(initial_variance_frequency)), integer(log2(initial_variance_frequency)));
  constant variance_phase_ufixed             : ufixed := to_ufixed(variance_phase, -1, -10);
  constant variance_frequency_ufixed         : ufixed := to_ufixed(variance_frequency, integer(log2(variance_frequency)), integer(log2(variance_frequency)));

begin
  assert variance_phase >= 0.0 report "Phase variance must not be negative" severity failure;
  assert rel_conv_error < 1.0e-2 report "Variance conversion failed. conv_error: " & real'image(rel_conv_error) severity failure;
  assert variance_frequency >= 0.0 report "Frequency variance must not be negative" severity failure;
  assert to_real(variance_frequency_ufixed) = variance_frequency report "Frequency variance conversion failed" severity failure;
  covariance_proc : process (clk, rst)
    type t_state is (APRIORI, APOSTERIORI);
    variable state               : t_state;
    variable aposteriori_coeff_0 : ufixed(1 downto kalman_gain_phase'right);
    variable aposteriori_coeff_1 : ufixed(1 downto kalman_gain_frequency'right);

  begin
    if rst then
      state := APRIORI;

      apriori_00 <= (others => '0');
      apriori_01 <= (others => '0');
      apriori_10 <= (others => '0');
      apriori_11 <= (others => '0');
      apriori_valid <= '0';

      aposteriori_00 <= resize(initial_variance_phase_ufixed, aposteriori_00'left, aposteriori_00'right);
      aposteriori_01 <= (others => '0');
      aposteriori_10 <= (others => '0');
      aposteriori_11 <= resize(initial_variance_frequency_ufixed, aposteriori_11'left, aposteriori_11'right);
      aposteriori_valid <= '0';

    elsif rising_edge(clk) then
      if state = APRIORI then
        apriori_00 <= resize(aposteriori_00 + aposteriori_01 + aposteriori_10 + aposteriori_11 + variance_phase_ufixed, apriori_00);
        apriori_01 <= resize(aposteriori_01 + aposteriori_11, apriori_01);
        apriori_10 <= resize(aposteriori_10 + aposteriori_11, apriori_10);
        apriori_11 <= resize(aposteriori_11 + variance_frequency_ufixed, apriori_11);
        apriori_valid <= '1';
        aposteriori_valid <= '0';
        state := APOSTERIORI;
      elsif state = APOSTERIORI then
        if input_valid then
          aposteriori_coeff_0 := to_ufixed(1,0,0) - kalman_gain_phase;
          aposteriori_coeff_1 := to_ufixed(1,0,0) - kalman_gain_frequency;
          aposteriori_00 <= resize(aposteriori_coeff_0 * apriori_00, aposteriori_00);
          aposteriori_01 <= resize(aposteriori_coeff_0 * apriori_01, aposteriori_01);
          aposteriori_10 <= resize(aposteriori_coeff_1 * apriori_10, aposteriori_10);
          aposteriori_11 <= resize(aposteriori_coeff_1 * apriori_11, aposteriori_11);
          apriori_valid <= '0';
          aposteriori_valid <= '1';
          state := APRIORI;
        end if;
      end if;
    end if;
  end process;

end covariance_estimator_arch;
