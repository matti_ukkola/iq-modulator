library ieee;
use ieee.fixed_float_types.all;
use ieee.fixed_pkg.all;
use IEEE.math_real.all;
use ieee.std_logic_1164.all;
/*Using VHDL 2008!*/

entity phase_estimator is
  generic(
    n_integer_bits_phase                : natural                 := 0;
    n_fractional_bits_phase             : natural                 := 30;
    n_integer_bits_frequency_ppm        : natural                 := 10;
    n_fractional_bits_frequency_ppm     : natural                 := 10;

    n_symbols_per_update                : natural range 1 to 1024 := 1024
  );
  port(
    rst              : in     std_logic;
    clk              : in     std_logic;
    -- phase in symbol periods, [0, 1)
    phase            : in     std_logic_vector(n_integer_bits_phase + n_fractional_bits_phase - 1 downto 0);
    next_phase       : out    std_logic_vector(n_integer_bits_phase + n_fractional_bits_phase - 1 downto 0);
    next_phase_valid : out    std_logic;
    -- signed frequency in ppm, e.g. 1e6 * symbol periods / symbol period, fits [-1024, 1023]
    frequency_ppm    : in     std_logic_vector(n_integer_bits_frequency_ppm + n_fractional_bits_frequency_ppm downto 0);
    input_valid      : in     std_logic
  );
end entity;

architecture phase_estimator_arch of phase_estimator is
  constant ppm                    : real    := 1.0e6;
  constant delta_t                : real    := real(n_symbols_per_update) / ppm;
  constant delta_t_n_bits         : integer := integer(ceil(log2(real(ppm)))) + 20;
  constant delta_t_sfixed         : sfixed  := to_sfixed(delta_t, 0, -delta_t_n_bits);
  constant modulo                 : sfixed  := to_sfixed(2.0**n_integer_bits_phase, n_integer_bits_phase + 1, 0);
begin
  assert delta_t < 1.0 report "delta_t >= 1 --> delta_t_sfixed cannot represent the number" severity failure;
  assert n_fractional_bits_phase = 20 + n_fractional_bits_frequency_ppm report
    "Frequency is in ppm -> fractional bits for phase must be 20 bits more than for frequency" severity failure;
  assert n_integer_bits_phase = 0 report "Integer bits for phase must be 0" severity failure;

  process (clk, rst)
    -- ufixed(-1 downto -1) is fine -> 1 bit wide, 1 fractional bit
    variable phase_ufixed                     : ufixed(n_integer_bits_phase - 1 downto -n_fractional_bits_phase);
    variable phase_sfixed                     : sfixed(phase_ufixed'left + 1 downto phase_ufixed'right);
    variable next_phase_ufixed                : ufixed(phase_ufixed'left downto phase_ufixed'right);
    variable frequency_sfixed_ppm             : sfixed(n_integer_bits_frequency_ppm downto -n_fractional_bits_frequency_ppm);
    variable phase_change_sfixed              : sfixed(0 downto
      sfixed_low (frequency_sfixed_ppm,'*',delta_t_sfixed));
    variable next_phase_sfixed                : sfixed(sfixed_high (phase_change_sfixed,'+',phase_sfixed) downto
      sfixed_low (phase_change_sfixed,'+',phase_sfixed));
    variable next_phase_sfixed_resized        : sfixed(phase_sfixed'left downto phase_sfixed'right);
  begin
    assert phase'length = phase_ufixed'length report "Phase length mismatch" severity failure;
    assert next_phase'length = next_phase_ufixed'length report "Next phase length mismatch" severity failure;
    assert frequency_ppm'length = frequency_sfixed_ppm'length report "Frequency length mismatch" severity failure;

    if rst = '1' then
      phase_ufixed := to_ufixed(0.0, phase_ufixed);
      phase_sfixed := to_sfixed(0.0, phase_sfixed);
      frequency_sfixed_ppm := to_sfixed(0.0, frequency_sfixed_ppm);
      phase_change_sfixed := to_sfixed(0.0, phase_change_sfixed);
      next_phase_sfixed := to_sfixed(0.0, next_phase_sfixed);
      next_phase_sfixed_resized := to_sfixed(0.0, next_phase_sfixed_resized);
      next_phase_ufixed := to_ufixed(0.0, next_phase_ufixed);
      next_phase <= (others => '0');

    elsif rising_edge(clk) and input_valid = '1' then
      phase_ufixed := to_ufixed(phase, phase_ufixed);
      phase_sfixed := sfixed(resize(phase_ufixed, phase_ufixed'left+1, phase_ufixed'right));
      frequency_sfixed_ppm := sfixed(frequency_ppm);
      phase_change_sfixed := resize(frequency_sfixed_ppm * delta_t_sfixed, phase_change_sfixed, overflow_style => fixed_wrap);
      next_phase_sfixed := phase_change_sfixed + phase_sfixed;
      if next_phase_sfixed >= modulo then
        next_phase_sfixed := resize(next_phase_sfixed - modulo, next_phase_sfixed);
      elsif next_phase_sfixed < 0.0 then
        next_phase_sfixed := resize(next_phase_sfixed + modulo, next_phase_sfixed);
      end if;
      assert next_phase_sfixed >= 0.0 and next_phase_sfixed < modulo report "Next phase out of bounds" severity failure;
      next_phase_sfixed_resized := resize(next_phase_sfixed, next_phase_sfixed_resized);
      next_phase_ufixed := ufixed(next_phase_sfixed_resized(next_phase_sfixed_resized'left-1 downto next_phase_ufixed'right));
      next_phase <= to_std_logic_vector(next_phase_ufixed);
    end if;
  end process;
end phase_estimator_arch;