def to_float(value, n_frac_bits) -> float:
    return float(value) / (1 << n_frac_bits)

def to_fixed(value, n_frac_bits) -> int:
    return int(round(value * (1 << n_frac_bits)))

MAX_FRAC_BITS = 30
def to_fixed_unknown(value):
    wanted_value = value
    n_frac_bits = 0
    if value == int(value):
        return value, n_frac_bits
    for _ in range(MAX_FRAC_BITS + 1):
        value *= 2
        n_frac_bits += 1
        if value == int(value):
            return value, n_frac_bits
    closest = to_float(int(value), n_frac_bits)
    abs_err = abs(wanted_value - closest)
    raise ValueError(f"Could not find a fixed point representation. value: {wanted_value}, "
                     f"n_frac_bits: {n_frac_bits-1}, closest: {closest}\n"
                     f"abs error: {abs_err:.2e}, "
                     f"rel error: {abs_err/abs(wanted_value):.2e}")

class FigurateNumber:
    def __init__(self):
        self.val_00 = 0
        self.val_01 = 0
        self.val_10 = 0
        self.val_11 = 0

        self.nat = 0
        self.triangular = 0
        self.tetrahedral = 0
        self.f = 1e-6 # 1 ppm
        self.phase = 1/12 # variance of uniform distribution with b-a=1
    
    def update(self):
        self.tetrahedral += self.triangular # tetrahedral
        self.triangular += self.nat # tetrahedral
        self.nat += 1

        self.val_00 += self.phase + self.val_01 + self.val_10 + self.val_11 # 2*tetrahedral + triangular
        self.val_01 += self.val_11 # triangular
        self.val_10 += self.val_11 # triangular
        self.val_11 += self.f

def plot_covariance_bounds():
    tetra = FigurateNumber()
    with open("covariance_bounds_f0_1ppm_phase0_1div12.txt", "w") as f:
        f.write("i val_00 val_01 val_10 val_11\n")
        for i in range(10):
            tetra.update()
            f.write(f"{i} {tetra.val_00} {tetra.val_01} {tetra.val_10} {tetra.val_11}\n")

if __name__ == '__main__':
    # print(to_fixed_unknown(1/2**10))
    plot_covariance_bounds()
