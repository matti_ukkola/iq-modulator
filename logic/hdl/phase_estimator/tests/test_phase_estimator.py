import math

import cocotb
from cocotb.triggers import RisingEdge
from cocotb.clock import Clock

from helpers import to_fixed, to_float, to_fixed_unknown, MAX_FRAC_BITS

async def init(dut):
    c = Clock(dut.clk, 10, "ns")
    await cocotb.start(c.start())
    print(f"Running {c}")
    dut.rst.value = 1
    await RisingEdge(dut.clk)
    await RisingEdge(dut.clk)
    dut.rst.value = 0

@cocotb.test()
async def test_init(dut):
    await init(dut)
    assert dut.next_phase.value == 0

@cocotb.test()
async def test_0_assignment(dut):
    await init(dut)
    dut.phase.value = 0
    dut.frequency_ppm.value = 0
    dut.input_valid.value = 1
    await RisingEdge(dut.clk)
    dut.input_valid.value = 0
    assert dut.next_phase.value == 0

TEST_FREQUENCIES = [-1024, -1023, -1000, -100, -10, -1, -0.1, 0, 0.1, 1, 10, 100, 1000, 1023]
ABSOLUTE_ERRORS = {"1024_symbols_per_update": 1e-6,
                   "512_symbols_per_update": 0.5e-6,
                   "256_symbols_per_update": 0.25e-6,
                   "128_symbols_per_update": 0.125e-6,
                   "64_symbols_per_update": 1e-7,
                   "32_symbols_per_update": 0.5e-7,
                   "16_symbols_per_update": 0.25e-7,
                   "8_symbols_per_update": 1e-8,
                   "4_symbols_per_update": 0.5e-8,
                   "2_symbols_per_update": 0.25e-8}

async def test_frequency_and_phase(dut, frequency_ppm, phase):
    n_fractional_bits_phase = dut.n_fractional_bits_phase.value.integer
    assert n_fractional_bits_phase <= MAX_FRAC_BITS

    n_fractional_bits_frequency = dut.n_fractional_bits_frequency_ppm.value.integer
    dut.phase.value = to_fixed(phase,
                               n_frac_bits=n_fractional_bits_phase)
    dut.frequency_ppm.value = to_fixed(frequency_ppm,
                                       n_frac_bits=n_fractional_bits_frequency)
    dut.input_valid.value = 1
    await RisingEdge(dut.clk)
    dut.input_valid.value = 0
    await RisingEdge(dut.clk)

    expected_phase = dut.n_symbols_per_update.value.integer * frequency_ppm / 1e6 % 1
    next_phase = to_float(dut.next_phase.value.integer, n_frac_bits=n_fractional_bits_phase)
    abs_tol = ABSOLUTE_ERRORS[f"{dut.n_symbols_per_update.value.integer}_symbols_per_update"]
    assert math.isclose(next_phase, expected_phase, rel_tol=1e-2) or math.isclose(next_phase, expected_phase + 1, rel_tol=1e-2)
    assert math.isclose(next_phase, expected_phase, abs_tol=abs_tol) or math.isclose(next_phase, expected_phase + 1, abs_tol=abs_tol)


@cocotb.test()
async def test_frequency_ppm_phase_0(dut):
    await init(dut)

    for frequency_ppm in TEST_FREQUENCIES:
        await test_frequency_and_phase(dut, frequency_ppm=frequency_ppm, phase=0)

@cocotb.test()
async def test_frequency_ppm_phase_1(dut):
    await init(dut)

    n_fractional_bits = dut.n_fractional_bits_phase.value.integer
    for frequency_ppm in TEST_FREQUENCIES:
        await test_frequency_and_phase(dut, frequency_ppm=frequency_ppm, phase=1-0.5**n_fractional_bits)

@cocotb.test()
async def test_frequency_ppm_1024_fails_nsymbols_1024(dut):
    await init(dut)

    frequency_ppm = 1024
    try:
        await test_frequency_and_phase(dut, frequency_ppm=frequency_ppm, phase=0)
    except AssertionError:
        pass
    else:
        assert False, f"Should have raised ValueError for frequency_ppm={frequency_ppm}"

    n_fractional_bits = dut.n_fractional_bits_phase.value.integer
    try:
        await test_frequency_and_phase(dut, frequency_ppm=frequency_ppm, phase=1-0.5**n_fractional_bits)
    except AssertionError:
        pass
    else:
        assert False, f"Should have raised ValueError for frequency_ppm={frequency_ppm}"

@cocotb.test()
async def test_to_fixed(dut):
    _, n_frac_bits = to_fixed_unknown(0.5)
    assert n_frac_bits == 1

    try:
        _ = to_fixed_unknown(0.1)
        assert False, "Should have raised ValueError"
    except ValueError:
        pass
