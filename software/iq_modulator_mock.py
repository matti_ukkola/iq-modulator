"""Defines the expected behaviour of the IQ Modulator Interface"""

from console_tool import check_crc

class IQModulator:
    """Mocked IQ Modulator"""

    HEADER_SIZE = 2
    MAX_PAYLOAD_SIZE = 10000
    CRC_SIZE = 2
    MAX_SIZE = HEADER_SIZE + MAX_PAYLOAD_SIZE + CRC_SIZE

    def __init__(self):
        self._rx_buffer = bytes()
        self._tx_buffer = bytes()
        self._payload_len = 0
        print("IQ Modulator mock initialized")

    def _crc_ok(self):
        assert len(self._rx_buffer) >= IQModulator.HEADER_SIZE + IQModulator.CRC_SIZE
        payload_bytes = self._rx_buffer[:-2]
        crc_bytes = self._rx_buffer[-2:]
        return check_crc(payload_bytes, crc_bytes)

    def _on_write(self):
        """Handle new buffer"""
        if len(self._rx_buffer) == IQModulator.HEADER_SIZE:
            self._payload_len = int.from_bytes(self._rx_buffer[0:2], byteorder="little")
            if self._payload_len > IQModulator.MAX_PAYLOAD_SIZE:
                self._payload_len = 0
                self._rx_buffer = b""
                self._tx_buffer = b"Bad payload size\r\n"
            else:
                self._tx_buffer = b"Payload size received\r\n"

        full_msg_size = (
            IQModulator.HEADER_SIZE + self._payload_len + IQModulator.CRC_SIZE
        )

        if len(self._rx_buffer) == full_msg_size:
            if self._crc_ok():
                self._tx_buffer += b"Transferred to fabric\r\n"
                self._rx_buffer = b""
                self._payload_len = 0
            else:
                self._tx_buffer += b"Bad CRC\r\n"
                self._rx_buffer = b""
                self._payload_len = 0

    def write_byte(self):
        """Send first byte of buffer. Shift rest."""
        if len(self._tx_buffer) == 0:
            return None
        b = self._tx_buffer[0]
        self._tx_buffer = self._tx_buffer[1:]
        return b.to_bytes(length=1, byteorder="little")

    def read_byte(self, b: bytes):
        """Write a single byte into buffer"""
        assert len(b) == 1

        if len(self._rx_buffer) < IQModulator.MAX_SIZE:
            self._rx_buffer += b
        self._on_write()
