#include <algorithm>
#include <cstdint>
#include <span>

#include "system_m2sxxx.h"

// NOLINTBEGIN(cert-dcl37-c, cert-dcl51-cpp, bugprone-reserved-identifier,
// cppcoreguidelines-avoid-non-const-global-variables)
// Replace with own isr vector table
extern "C" const uint32_t __vector_table_load;
extern "C" uint32_t __vector_table_start;
extern "C" const uint32_t _evector_table;

extern "C" const uint32_t __text_load;
extern "C" uint32_t __text_start;
extern "C" const uint32_t _etext;

extern "C" const uint32_t __data_load;
extern "C" uint32_t __data_start;
extern "C" uint32_t _edata;

extern "C" uint32_t __bss_start__;
extern "C" uint32_t __bss_end__;

extern "C" const uint32_t __main_stack_start;
extern "C" const uint32_t _estack;

using InitFunctionPtr = void (*)();
extern "C" InitFunctionPtr __init_array_start;
extern "C" InitFunctionPtr __init_array_end;

// NOLINTEND(cert-dcl37-c, cert-dcl51-cpp, bugprone-reserved-identifier,
// cppcoreguidelines-avoid-non-const-global-variables)

constexpr uint32_t RAM_INIT_PATTERN = 0;
constexpr bool MIRRORED_NVM =
    true;// if true, the NVM is mirrored to the RAM and thus vector table text section are not copied!

extern "C" void mscc_post_hw_cfg_init() {}

extern int main();

extern "C" [[noreturn, gnu::section(".boot_code"), gnu::used, gnu::naked]] void Reset_Handler()
{
    asm("ldr sp, =_estack");// NOLINT(hicpp-no-assembler) Needed to set the stack pointer
    asm("b _start");// NOLINT(hicpp-no-assembler) Needed to call _start
}

extern "C" [[noreturn, gnu::section(".boot_code"), gnu::used]] void _start()
{
    SystemInit();// fabric fixes, must be called before relocation

    if constexpr (!MIRRORED_NVM) {
        std::span<uint32_t> vector_table_dest{ &__vector_table_start, &_evector_table };
        std::span<const uint32_t> vector_table_source{ &__vector_table_load, vector_table_dest.size() };
        std::ranges::copy(vector_table_source, vector_table_dest.begin());

        std::span<uint32_t> text_destination{ &__text_start, &_etext };
        std::span<const uint32_t> text_source{ &__text_load, text_destination.size() };
        std::ranges::copy(text_source, text_destination.begin());
    }
    std::span<uint32_t> bss{ &__bss_start__, &__bss_end__ };
    std::ranges::fill(bss, RAM_INIT_PATTERN);

    std::span<uint32_t> data_destination{ &__data_start, &_edata };
    std::span<const uint32_t> data_source{ &__data_load, data_destination.size() };
    std::ranges::copy(data_source, data_destination.begin());

    std::span<InitFunctionPtr> global_constructors{ &__init_array_start, &__init_array_end };
    for (const InitFunctionPtr& constructor : global_constructors) { constructor(); }

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wmain"
#pragma GCC diagnostic ignored "-Wpedantic"
    main();
#pragma GCC diagnostic pop
    while (true) {}
}

[[gnu::used]] void default_handler()
{
    while (true) {}
}

[[gnu::used]] void default_handler_hard_fault()
{
    while (true) {}
}

extern "C" void UART0_IRQHandler();

struct IRQTable
{
    uint32_t stack_end = _estack;
    void (*reset_handler)() = Reset_Handler;
    void (*nmi_handler)() = default_handler;
    void (*hard_fault_handler)() = default_handler_hard_fault;
    void (*mem_manage_handler)() = default_handler;
    void (*bus_fault_handler)() = default_handler;
    void (*usage_fault_handler)() = default_handler;
    void (*unused_handler_0)() = nullptr;
    void (*unused_handler_1)() = nullptr;
    void (*unused_handler_2)() = nullptr;
    void (*unused_handler_3)() = nullptr;
    void (*sv_call_handler)() = default_handler;
    void (*debug_monitor_handler)() = default_handler;
    void (*unused_handler_4)() = nullptr;
    void (*pend_sv_handler)() = default_handler;
    void (*systick_handler)() = default_handler;
    void (*wdog_wake_up_irq_handler)() = default_handler;
    void (*rtc_wakeup_irq_handler)() = default_handler;
    void (*spi0_irq_handler)() = default_handler;
    void (*spi1_irq_handler)() = default_handler;
    void (*i2c0_irq_handler)() = default_handler;
    void (*i2c0_smb_alert_irq_handler)() = default_handler;
    void (*i2c0_smbus_irq_handler)() = default_handler;
    void (*i2c1_irq_handler)() = default_handler;
    void (*i2c1_smb_alert_irq_handler)() = default_handler;
    void (*i2c1_smbus_irq_handler)() = default_handler;
    void (*uart0_irq_handler)() = UART0_IRQHandler;
    void (*uart1_irq_handler)() = default_handler;
    void (*ethernet_mac_irq_handler)() = default_handler;
    void (*dma_irq_handler)() = default_handler;
    void (*timer1_irq_handler)() = default_handler;
    void (*timer2_irq_handler)() = default_handler;
    void (*can_irq_handler)() = default_handler;
    void (*envm0_irq_handler)() = default_handler;
    void (*envm1_irq_handler)() = default_handler;
    void (*com_blk_irq_handler)() = default_handler;
    void (*usb_irq_handler)() = default_handler;
    void (*usb_dma_irq_handler)() = default_handler;
    void (*pll_lock_irq_handler)() = default_handler;
    void (*pll_lock_lost_irq_handler)() = default_handler;
    void (*comm_switch_error_irq_handler)() = default_handler;
    void (*cache_error_irq_handler)() = default_handler;
    void (*hpdma_complete_irq_handler)() = default_handler;
    void (*hpdma_error_irq_handler)() = default_handler;
    void (*ecc_error_irq_handler)() = default_handler;
    void (*mddr_iocalib_irq_handler)() = default_handler;
    void (*fab_pll_lock_irq_handler)() = default_handler;
    void (*fab_pll_lock_lost_irq_handler)() = default_handler;
    void (*fic64_irq_handler)() = default_handler;
    void (*fabric_irq0_irq_handler)() = default_handler;
    void (*fabric_irq1_irq_handler)() = default_handler;
    void (*fabric_irq2_irq_handler)() = default_handler;
    void (*fabric_irq3_irq_handler)() = default_handler;
    void (*fabric_irq4_irq_handler)() = default_handler;
    void (*fabric_irq5_irq_handler)() = default_handler;
    void (*fabric_irq6_irq_handler)() = default_handler;
    void (*fabric_irq7_irq_handler)() = default_handler;
    void (*fabric_irq8_irq_handler)() = default_handler;
    void (*fabric_irq9_irq_handler)() = default_handler;
    void (*fabric_irq10_irq_handler)() = default_handler;
    void (*fabric_irq11_irq_handler)() = default_handler;
    void (*fabric_irq12_irq_handler)() = default_handler;
    void (*fabric_irq13_irq_handler)() = default_handler;
    void (*fabric_irq14_irq_handler)() = default_handler;
    void (*fabric_irq15_irq_handler)() = default_handler;
    void (*gpio0_0_irq_handler)() = default_handler;
    void (*gpio0_1_irq_handler)() = default_handler;
    void (*gpio0_2_irq_handler)() = default_handler;
    void (*gpio0_3_irq_handler)() = default_handler;
    void (*gpio0_4_irq_handler)() = default_handler;
    void (*gpio0_5_irq_handler)() = default_handler;
    void (*gpio0_6_irq_handler)() = default_handler;
    void (*gpio0_7_irq_handler)() = default_handler;
    void (*gpio0_8_irq_handler)() = default_handler;
    void (*gpio0_9_irq_handler)() = default_handler;
    void (*gpio0_10_irq_handler)() = default_handler;
    void (*gpio0_11_irq_handler)() = default_handler;
    void (*gpio0_12_irq_handler)() = default_handler;
    void (*gpio0_13_irq_handler)() = default_handler;
    void (*gpio0_14_irq_handler)() = default_handler;
    void (*gpio0_15_irq_handler)() = default_handler;
    void (*gpio0_16_irq_handler)() = default_handler;
    void (*gpio0_17_irq_handler)() = default_handler;
    void (*gpio0_18_irq_handler)() = default_handler;
    void (*gpio0_19_irq_handler)() = default_handler;
    void (*gpio0_20_irq_handler)() = default_handler;
    void (*gpio0_21_irq_handler)() = default_handler;
    void (*gpio0_22_irq_handler)() = default_handler;
    void (*gpio0_23_irq_handler)() = default_handler;
    void (*gpio0_24_irq_handler)() = default_handler;
    void (*gpio0_25_irq_handler)() = default_handler;
    void (*gpio0_26_irq_handler)() = default_handler;
    void (*gpio0_27_irq_handler)() = default_handler;
    void (*gpio0_28_irq_handler)() = default_handler;
    void (*gpio0_29_irq_handler)() = default_handler;
    void (*gpio0_30_irq_handler)() = default_handler;
    void (*gpio0_31_irq_handler)() = default_handler;
    void (*unused_handler_5)() = nullptr;
    void (*unused_handler_6)() = nullptr;
};

extern "C" [[gnu::section(".isr_vector"), gnu::used]] const IRQTable IRQ_TABLE{};
