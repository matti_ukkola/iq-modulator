# Example

```cpp
uint64_t val = 0x1122334455667788; // little-endian system
auto arr = std::bit_cast<std::array<uint8_t, 8>>(val);
std::cout << std::hex << int(arr[0]) << std::endl; // prints 0x88
std::cout << std::hex << int(arr[1]) << std::endl; // prints 0x77
std::cout << std::hex << int(arr[2]) << std::endl; // prints 0x66
std::cout << std::hex << int(arr[3]) << std::endl; // prints 0x55
std::cout << std::hex << int(arr[4]) << std::endl; // prints 0x44
std::cout << std::hex << int(arr[5]) << std::endl; // prints 0x33
std::cout << std::hex << int(arr[6]) << std::endl; // prints 0x22
std::cout << std::hex << int(arr[7]) << std::endl; // prints 0x11
std::array<uint8_t, 4> arr2 {};
std::memcpy(arr2.data(), &val, 4);
std::cout << std::hex << int(arr2[0]) << std::endl; // prints 0x88
std::cout << std::hex << int(arr2[1]) << std::endl; // prints 0x77
std::cout << std::hex << int(arr2[2]) << std::endl; // prints 0x66
std::cout << std::hex << int(arr2[3]) << std::endl; // prints 0x55
```

In VHDL we save the value as little endian with `L downto R`:
```vhdl
tx_bits : std_logic_vector(31 downto 0) := 0x55667788;
```

If we index into `tx_bits` with 0, we get the LSB, or the last bit of 0x88 first on the transmission line.
