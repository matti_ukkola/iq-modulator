#include <array>
#include <cstddef>
#include <cstdint>
#include <limits>
#include <span>

namespace iqmodulator::crc {

constexpr uint16_t CRC_POLY_CCITT = 0x1021;
constexpr uint16_t CRC_START_CCITT_XMODEM = 0x0000;
constexpr size_t CRC_CCITT_TABLE_SIZE = 256;

constexpr std::array<uint16_t, CRC_CCITT_TABLE_SIZE> calculate_crc_table()
{
    std::array<uint16_t, CRC_CCITT_TABLE_SIZE> crc_tab{};

    uint16_t itr = 0;
    for (auto& crc : crc_tab) {
        constexpr uint8_t BITS = 8;
        auto remainder = static_cast<uint16_t>(itr << BITS);
        for (uint16_t j = 0; j < BITS; j++) {
            constexpr uint16_t TOPBIT = 0x8000;
            if ((static_cast<uint16_t>(crc ^ remainder) & TOPBIT) != 0) {
                crc = static_cast<uint16_t>(crc << 1U) ^ CRC_POLY_CCITT;
            } else {
                crc = static_cast<uint16_t>(crc << 1U);
            }
            remainder = static_cast<uint16_t>(remainder << 1U);
        }
        ++itr;
    }
    return crc_tab;
}

constexpr std::array<uint16_t, CRC_CCITT_TABLE_SIZE> CRC_CCITT_TABLE = calculate_crc_table();

constexpr uint16_t crc_ccitt_xmodem(std::span<const std::byte> data)
{
    constexpr unsigned mask_lower = 0x00ff;
    constexpr uint8_t shift_byte = 8;

    uint16_t crc = CRC_START_CCITT_XMODEM;
    for (const auto& byte : data) {
        const uint16_t val = mask_lower & static_cast<uint16_t>(byte);
        const auto crc_index = static_cast<uint8_t>(static_cast<uint16_t>(crc >> shift_byte) ^ val);
        static_assert(std::numeric_limits<decltype(crc_index)>::max() <= CRC_CCITT_TABLE.size());
        // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-constant-array-index) Index checked statically above
        crc = static_cast<uint16_t>(crc << shift_byte) ^ CRC_CCITT_TABLE[crc_index];
    }
    return crc;
}
}// namespace iqmodulator::crc