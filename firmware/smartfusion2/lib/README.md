# CMSIS

## Licensing

`CMSIS_4` is licensed under a permissive license, Apache 2 for newer CMSIS versions. However, the `drivers`, `drivers_config` and `CMSIS` folders need to be exported from `Libero`, the FPGA development IDE. 
