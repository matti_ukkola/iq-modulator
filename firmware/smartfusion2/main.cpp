#include <cstddef>
#include <cstdint>
#include <cstring>
#include <span>
#include <string_view>

#include "mss_uart/mss_uart.h"

#include "crc.hpp"

void print(std::string_view msg)
{
    // NOLINTNEXTLINE(cppcoreguidelines-pro-type-reinterpret-cast)
    MSS_UART_polled_tx(&g_mss_uart0, reinterpret_cast<const uint8_t*>(msg.data()), static_cast<uint32_t>(msg.size()));
}

void transfer_32_bit_to_fabric(uint32_t data)
{
    static constexpr uint32_t REG_ADDR = 0x30000000;
    // NOLINTNEXTLINE(cppcoreguidelines-pro-type-reinterpret-cast, performance-no-int-to-ptr)
    auto* addr = reinterpret_cast<uint32_t*>(REG_ADDR);
    *addr = data;
}

size_t transfer_to_fabric(std::span<const uint8_t> buffer)
{
    size_t sent_bytes = 0;
    while (sent_bytes < buffer.size()) {
        uint32_t next_bytes = 0;
        size_t bytes_left = buffer.size() - sent_bytes;
        size_t transfer_size = (bytes_left < sizeof(next_bytes)) ? bytes_left : sizeof(next_bytes);
        std::memcpy(&next_bytes, &buffer[sent_bytes], transfer_size);
        // TODO: add rate limitation
        transfer_32_bit_to_fabric(next_bytes);
        sent_bytes += sizeof(next_bytes);
    }
    return sent_bytes;
}

struct State
{
    uint16_t payload_size = 0;
    static constexpr size_t MAX_PAYLOAD_SIZE = 10000;
    std::array<uint8_t, sizeof(payload_size) + MAX_PAYLOAD_SIZE + sizeof(uint16_t)> read_buf{};
    size_t read_buf_size = 0;

    void reset()
    {
        read_buf_size = 0;
        payload_size = 0;
    }

    void read(uint8_t rx_byte)
    {
        if (read_buf_size >= read_buf.max_size()) { return; }

        // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-constant-array-index) - read_buf_size is checked
        read_buf[read_buf_size] = rx_byte;
        ++read_buf_size;
        if (read_buf_size == sizeof(payload_size)) {
            std::memcpy(&payload_size, read_buf.data(), sizeof(payload_size));
            if (payload_size > MAX_PAYLOAD_SIZE) {
                print("Bad payload size\r\n");
                reset();
            } else {
                print("Payload size received\r\n");
            }
        }
        constexpr size_t CRC_SIZE = sizeof(uint16_t);
        size_t full_msg_size = sizeof(payload_size) + payload_size + CRC_SIZE;
        if (read_buf_size == full_msg_size) {
            uint16_t crc16 = 0;
            // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-constant-array-index) - read_buf_size is checked
            std::memcpy(&crc16, &read_buf[read_buf_size - 2], sizeof(crc16));
            std::span<const uint8_t> data{ read_buf.data(), read_buf_size - 2 };
            uint16_t checked_crc = iqmodulator::crc::crc_ccitt_xmodem(std::as_bytes(data));
            if (crc16 == checked_crc) {
                size_t bytes_transferred = transfer_to_fabric(std::span{ read_buf.data(), read_buf_size - 2 });
                if (bytes_transferred != 0) {
                    print("Transferred to fabric\r\n");
                } else {
                    print("Transfer to fabric failed\r\n");
                }
            } else {
                print("Bad CRC\r\n");
            }
            reset();
        }
    }
};

// NOLINTNEXTLINE(cppcoreguidelines-avoid-non-const-global-variables) Global state in MSS
State g_state{};

void uart0_rx_handler([[maybe_unused]] mss_uart_instance* mss_uart_ptr)
{
    uint8_t rx_byte = 0;
    size_t rx_size = MSS_UART_get_rx(&g_mss_uart0, &rx_byte, 1);
    if (rx_size == 1) { g_state.read(rx_byte); }
}

void init_uart()
{
    // NOLINTNEXTLINE(misc-redundant-expression)
    MSS_UART_init(&g_mss_uart0, MSS_UART_57600_BAUD, MSS_UART_DATA_8_BITS | MSS_UART_NO_PARITY | MSS_UART_ONE_STOP_BIT);
    MSS_UART_set_rx_handler(&g_mss_uart0, uart0_rx_handler, MSS_UART_FIFO_SINGLE_BYTE);
}

int main()
{
    init_uart();
    print("UART initialized\r\n");
    for (;;) {}
    return 0;
}
