# prevents cmake building host-based executable as test
set(CMAKE_TRY_COMPILE_TARGET_TYPE STATIC_LIBRARY)

set(CMAKE_AR                    arm-none-eabi-ar)
set(CMAKE_ASM_COMPILER          arm-none-eabi-as)
set(CMAKE_C_COMPILER            arm-none-eabi-gcc)
set(CMAKE_CXX_COMPILER          arm-none-eabi-g++)
set(CMAKE_LINKER                arm-none-eabi-ld)
set(CMAKE_OBJCOPY               arm-none-eabi-objcopy)
set(CMAKE_RANLIB                arm-none-eabi-ranlib)
set(CMAKE_SIZE                  arm-none-eabi-size)
set(CMAKE_STRIP                 arm-none-eabi-strip)

set(CMAKE_SYSTEM_NAME           Generic)
set(CMAKE_SYSTEM_PROCESSOR      cortex-m3)

set(CPU_OPTIONS "-mcpu=cortex-m3 -mthumb")

# Perform garbage collection of code and data never referenced.
set(CMAKE_LINKER_FLAGS "--gc-sections -nostartfiles")
set(CMAKE_C_FLAGS "${CPU_OPTIONS} -fdata-sections -ffunction-sections -Wl,${CMAKE_LINKER_FLAGS}" CACHE INTERNAL "")
set(CMAKE_CXX_FLAGS "${CMAKE_C_FLAGS} -fno-exceptions -fno-rtti" CACHE INTERNAL "")

# note: -0s turns off -falign-functions, which in startup code causes a crash when calling global constructors
# https://gcc.gnu.org/onlinedocs/gcc/Optimize-Options.html
set(CMAKE_C_FLAGS_DEBUG "-O3 -g3" CACHE INTERNAL "")
set(CMAKE_C_FLAGS_RELEASE "-O3 -g3" CACHE INTERNAL "")
set(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_C_FLAGS_DEBUG}" CACHE INTERNAL "")
set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_C_FLAGS_RELEASE}" CACHE INTERNAL "")

set(CMAKE_EXECUTABLE_SUFFIX_C .elf)
set(CMAKE_EXECUTABLE_SUFFIX_CXX .elf)
